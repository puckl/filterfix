<?php
/*
 *   *********************************************************************************************
 *      Please retain this copyright header in all versions of the software.
 *      Bitte belassen Sie diesen Copyright-Header in allen Versionen der Software.
 *
 *      Copyright (C) Josef A. Puckl | eComStyle.de
 *      All rights reserved - Alle Rechte vorbehalten
 *
 *      This commercial product must be properly licensed before being used!
 *      Please contact info@ecomstyle.de for more information.
 *
 *      Dieses kommerzielle Produkt muss vor der Verwendung ordnungsgemäß lizenziert werden!
 *      Bitte kontaktieren Sie info@ecomstyle.de für weitere Informationen.
 *   *********************************************************************************************
 */

$sMetadataVersion   = '2.0';
$aModule            = [
    'id'            => 'ecs_filterfix',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>FilterFix</i>',
    'description'   => 'Fix fuer den Attributfilter, Auswahl bleibt auch beim Seitenwechsel erhalten, Zuruecksetzen funktioniert.',
    'version'       => '1.0.1',
    'thumbnail'     => 'ecs.png',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'info@ecomstyle.de',
    'url'           => 'https://ecomstyle.de',

    'extend' => [
        \OxidEsales\Eshop\Core\Session::class                                 => Ecs\FilterFix\Core\Session::class,
        \OxidEsales\Eshop\Application\Controller\ArticleListController::class => Ecs\FilterFix\Controller\ArticleListController::class,
    ],
    'events' => [
        'onActivate'    => '\Ecs\FilterFix\Core\Events::onActivate',
        'onDeactivate'  => '\Ecs\FilterFix\Core\Events::onDeactivate',
    ],
];
