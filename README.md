eComStyle.de::FilterFix
==========================
Fix fuer den Attributfilter, Auswahl bleibt auch beim Seitenwechsel erhalten, Zuruecksetzen funktioniert.
  
#### Composer name
`ecs/filterfix`


#### Shopversionen
`OXID eShop CE/PE 6`


#### Installation Produktiv-Umgebung
`composer require ecs/filterfix --update-no-dev`


#### Installation Dev-Umgebung
`composer require ecs/filterfix`
